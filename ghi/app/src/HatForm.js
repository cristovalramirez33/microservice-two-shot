import React from "react";

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: "",
            style: "",
            color: "",
            locations: []
        }
        this.handleFabricChange = this.handleFabricChange.bind(this)
        this.handleStyleChange = this.handleStyleChange.bind(this)
        this.handleColorChange = this.handleColorChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    handleFabricChange(event) {
        const value = event.target.value
        this.setState({fabric: value})
    }
    handleStyleChange(event) {
        const value = event.target.value
        this.setState({style: value})
    }
    handleColorChange(event) {
        const value = event.target.value
        this.setState({color: value})
    }
    handleLocationChange(event) {
        const value = event.target.value
        this.setState({location: value})
    }
    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.locations;
        console.log(data);
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(hatUrl, fetchConfig);

    if (response.ok) {
        const newHat = await response.json();
        console.log(newHat);

        const cleared = {
            fabric: "",
            style: "",
            color: "",
            location: ''
        };
        this.setState(cleared);
      }
  }

        async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});


        }
      }


render() {
    return (
<div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={this.state.fabric} onChange={this.handleFabricChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Start date" name="style" type="text" id="style" className="form-control"/>
                <label htmlFor="starts">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input value={this.state.color} onChange={this.handleColorChange} placeholder="End date" name="color" type="text" id="color" className="form-control"/>
                <label htmlFor="ends">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                        <option key={location.href} value={location.href}>
                        {location.closet_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

}

export default HatForm
