import './App.css'
import React, { Component } from 'react'

class HatList extends React.Component{
  constructor() {
    super()
    this.state = {
      "hats": []
    }
    this.handleDeleteClick = this.handleDeleteClick.bind(this)
  }


async componentDidMount() {
  const url = 'http://localhost:8090/api/hats'
  let response = await fetch(url)

  if (response.ok) {
    let data = await response.json()
    this.setState({"hats":data.hats})
  }
}

async handleDeleteClick(event) {
  console.log("remove")
  // every time you click button, pass in id
  // create id variable
  // fetch on delete to
  const id = event.target.value
  const url = `http://localhost:8090/api/hats/${id}/`;
  const fetchConfig = {
    method: "DELETE",
    headers: {
      'Content-Type': 'application.json'
    }
  }
  const response = await fetch(url, fetchConfig)
  if (response.ok) {
    // setting the state for the page to only display hats where the hat id is not the current id
    this.setState({hats: this.state.hats.filter(hat => hat.id != id)})
  }

}


render () {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Location</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.state.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td><button onClick={ this.handleDeleteClick} value = {hat.id} className="btn btn-outline-danger btn-sm">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
  }
}
  export default HatList;
