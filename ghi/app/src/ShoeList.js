import './App.css'
import React, { Component } from 'react'


class ShoeList extends React.Component{
  constructor() {
    super()
    this.state = {
      "shoes": []
    }
    this.handleDeleteClick = this.handleDeleteClick.bind(this)
}

async componentDidMount() {
  const url = 'http://localhost:8080/api/shoes'
  let response = await fetch(url)

  if (response.ok) {
    let data = await response.json()
    this.setState({"shoes":data.shoes})
  }
}

async handleDeleteClick(event) {
  console.log("remove")
  const id = event.target.value
  const url = `http://localhost:8080/api/shoes/${id}/`;
  const fetchConfig = {
    method: "DELETE",
    headers: {
      'Content-Type':'application-json'
    }
  }
  const response = await fetch(url, fetchConfig)
  if (response.ok) {
    this.setState({shoes: this.state.shoes.filter(shoe => shoe.id != id)})
  }
  
}
    render () {
      return (
        <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            {/* <th>Picture</th> */}
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {this.state.shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                {/* <td>{ shoe.picture_url }</td> */}
                <td>{ shoe.bin }</td>
                <td><button onClick={ this.handleDeleteClick} value = {shoe.id} className="btn btn-outline-danger btn-sm">Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
  }
}

  export default ShoeList;
